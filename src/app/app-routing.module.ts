import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// layouts
import { AdminComponent } from "./layouts/admin/admin.component";
import { AuthComponent } from "./layouts/auth/auth.component";

// admin views
import { DashboardComponent } from "./views/admin/dashboard/dashboard.component";
import { MapsComponent } from "./views/admin/maps/maps.component";
import { SettingsComponent } from "./views/admin/settings/settings.component";
import { TablesComponent } from "./views/admin/tables/tables.component";

// auth views
import { SapSignupComponent } from "./views/auth/sap-signup/sap-signup.component";
import { LoginComponent } from "./views/auth/login/login.component";
import { RegisterComponent } from "./views/auth/register/register.component";
import { PlansComponent } from "./views/auth/plans/plans.component";

// no layouts views
import { IndexComponent } from "./views/index/index.component";
import { LandingComponent } from "./views/landing/landing.component";
import { ProfileComponent } from "./views/profile/profile.component";
import { AuthGuard } from './core/_helpers';


import { AddProductComponent } from "./views/admin/products/add-product/add-product.component";
import { ListProductsComponent } from "./views/admin/products/list-products/list-products.component";
import { RewardsHomeComponent } from "./views/admin/rewards/rewards-home/rewards-home.component";
import { LoyaltyPointsComponent } from "./views/admin/rewards/loyalty-points/loyalty-points.component";
import { AirtimeRewardComponent } from "./views/admin/rewards/airtime-reward/airtime-reward.component";
import { MerchandizeRewardComponent } from "./views/admin/rewards/merchandize-reward/merchandize-reward.component";
import { SurveysRoutingModule } from "./views/admin/surveys/surveys-routing.module";
import { SurveyHomeComponent } from "./views/admin/surveys/survey-home/survey-home.component";
import { CreateSurveyComponent } from "./views/admin/surveys/create-survey/create-survey.component";
import { CollectionHomeComponent } from "./views/admin/code-collections/collection-home/collection-home.component";
import { InsightsHomeComponent } from "./views/admin/insights/insights-home/insights-home.component";
import { EngageHomeComponent } from "./views/admin/engage/engage-home/engage-home.component";
import { UploadCsvComponent } from "./views/admin/engage/upload-csv/upload-csv.component";
import { TargetGroupComponent } from "./views/admin/engage/target-group/target-group.component";
import { ExistingCustomerComponent } from "./views/admin/engage/existing-customer/existing-customer.component";
import { AnalyticsSummaryComponent } from "./views/admin/analytics/analytics-summary/analytics-summary.component";
import { CreateLoyaltyRewardComponent } from './views/admin/rewards/create-loyalty-reward/create-loyalty-reward.component';
import { CreateAirtimeRewardComponent } from './views/admin/rewards/create-airtime-reward/create-airtime-reward.component';
import { CreateMerchandizeRewardComponent } from './views/admin/rewards/create-merchandize-reward/create-merchandize-reward.component';
import { RaffleDrawComponent } from './views/admin/rewards/raffle-draw/raffle-draw.component';
import { CreateRaffleRewardComponent } from './views/admin/rewards/create-raffle-reward/create-raffle-reward.component';
import { ProductBatchesComponent } from './views/admin/products/product-batches/product-batches.component';
import { CreateBatchComponent } from './views/admin/products/create-batch/create-batch.component';
import { NewPinComponent } from './views/admin/products/new-pin/new-pin.component';
import { ViewBatchComponent } from './views/admin/products/view-batch/view-batch.component';
import { ConnectComponent } from './views/sap/connect/connect.component';
import { RequestSerialComponent } from './views/sap/request-serial/request-serial.component';
import { SapHomeComponent } from './views/sap/sap-home/sap-home.component';
import { SapDashboardComponent } from './views/sap/sap-dashboard/sap-dashboard.component';

import { BatchComponent } from './views/sap/batch/batch.component';
import { SetupSurveysComponent } from './views/sap/setup-surveys/setup-surveys.component';
import { AttachRewardsComponent } from './views/sap/attach-rewards/attach-rewards.component';
import { AttachRewardComponent } from './views/sap/attach-reward/attach-reward.component';
import { SummaryComponent } from './views/sap/summary/summary.component';
import { LinkAccountComponent } from './views/sap/link-account/link-account.component';
import { SubscriptionComponent } from './views/sap/subscription/subscription.component';
import { RequestPinComponent } from './views/sap/request-pin/request-pin.component';
import { UniqueIdComponent } from './views/sap/unique-id/unique-id.component';
import { QuicklinksComponent } from './views/sap/quicklinks/quicklinks.component';
import { AssignSurveyComponent } from './views/sap/assign-survey/assign-survey.component';
import { AnalyticsComponent } from './views/sap/analytics/analytics.component';
import { ProductAnalyticsComponent } from './views/sap/product-analytics/product-analytics.component';

import { SapAssignSurveysComponent } from './views/sap/sap-assign-surveys/sap-assign-surveys.component';
import { SapAssignRewardsComponent } from './views/sap/sap-assign-rewards/sap-assign-rewards.component';



const routes: Routes = [
  // admin views
  {
    path: "dashboard",
    canActivate: [AuthGuard], 
    component: AdminComponent,
    children: [
      { path: "overview", component: DashboardComponent },
      { path: "settings", component: SettingsComponent },
      { path: "tables", component: TablesComponent },
      { path: "maps", component: MapsComponent },
      { path: "products", component: ListProductsComponent },
      { path: "products/product-batches", component: ProductBatchesComponent },
      { path: "products/create-batch", component: CreateBatchComponent },
      { path: "products/new-pin", component: NewPinComponent },
      { path: "products/view-batch", component: ViewBatchComponent },
      { path: "rewards", component: RewardsHomeComponent },
      { path: 'rewards/loyalty-points', component: LoyaltyPointsComponent},
      { path: 'rewards/create-loyalty-reward', component: CreateLoyaltyRewardComponent},
      { path: 'rewards/create-airtime-reward', component: CreateAirtimeRewardComponent},
      { path: 'rewards/create-merchandize-reward', component: CreateMerchandizeRewardComponent},
      { path: 'rewards/create-raffle-reward', component: CreateRaffleRewardComponent},
      { path: 'rewards/airtime', component: AirtimeRewardComponent},
      { path: 'rewards/raffle', component: RaffleDrawComponent},
      { path: 'rewards/merchandize', component: MerchandizeRewardComponent},
      { path: 'surveys', component: SurveyHomeComponent},
      { path: 'surveys/create-survey', component: CreateSurveyComponent},
      { path: 'code-collections', component: CollectionHomeComponent},
      { path: "products/add-product", component: AddProductComponent },
      { path: 'insights', component: InsightsHomeComponent},
      { path: 'engage', component: EngageHomeComponent},
      { path: 'engage/upload-csv', component: UploadCsvComponent},
      { path: 'engage/target-group', component: TargetGroupComponent},
      { path: 'engage/existing-contacts', component: ExistingCustomerComponent},
      { path: 'analytics', component: AnalyticsSummaryComponent},
      { path: "", redirectTo: "overview", pathMatch: "full" },
    ],
  },
  {
    path: "sap",
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      { path: "sap-assign-rewards", component: SapAssignRewardsComponent },
      { path: "sap-assign-surveys", component: SapAssignSurveysComponent },
      { path: "product-analytics", component: ProductAnalyticsComponent },
      { path: "analytics", component: AnalyticsComponent },
      { path: "assign-survey", component: AssignSurveyComponent },
      { path: "quicklinks", component: QuicklinksComponent },
      { path: "unique-id", component: UniqueIdComponent },
      { path: "subscription", component: SubscriptionComponent },
      { path: "link-account", component: LinkAccountComponent },
      { path: "summary", component: SummaryComponent },
      { path: "attach-reward", component: AttachRewardComponent },
      { path: "attach-rewards", component: AttachRewardsComponent },
      { path: "setup-surveys", component: SetupSurveysComponent },
      { path: "batch", component: BatchComponent },
      { path: "connect", component: ConnectComponent },
      { path: "home", component: SapHomeComponent },
      { path: "dashboard", component: SapDashboardComponent },
      { path: "request-serial", component: RequestSerialComponent },
      { path: "request-pin", component: RequestPinComponent },
      { path: "", redirectTo: "home", pathMatch: "full" },
    ],
  },
  // auth views /dashboard/products/view-batch
  {
    path: "authentication",
    component: AuthComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "register", component: RegisterComponent },
      { path: "sap-signup", component: SapSignupComponent },
      // { path: "plans", component: PlansComponent },
      { path: "", redirectTo: "login", pathMatch: "full" },
    ],
  },
  // no layout views
  { path: "authentication/plans", component: PlansComponent },
  { path: "profile", component: ProfileComponent },
  { path: "landing", component: LandingComponent },
  { path: "", component: IndexComponent },
  { path: "**", redirectTo: "", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
