import { Component, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';


@Component({
  selector: 'app-setup-surveys',
  templateUrl: './setup-surveys.component.html',
  styleUrls: ['./setup-surveys.component.css']
})
export class SetupSurveysComponent implements OnInit {

  constructor(private dragulaService: DragulaService) {
    // dragulaService.setOptions('bag-task1', {
    //   removeOnSpill: true
    // });
    // dragulaService.setOptions('bag-task2', {
    //   revertOnSpill: true
    // });
    // dragulaService.setOptions('bag-task3', {
    //   copy: true
    // });
   }

   ngOnInit() {
    this.dragulaService.createGroup('DRAGULA_CONTAINER', {});
  }

}
