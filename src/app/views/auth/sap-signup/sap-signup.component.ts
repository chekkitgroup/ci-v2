import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sap-signup',
  templateUrl: './sap-signup.component.html',
  styleUrls: ['./sap-signup.component.css']
})
export class SapSignupComponent implements OnInit {
  showPassword = false;
  passwordToggleIcon = 'eye';
  password: '';

  constructor() { }

  ngOnInit(): void {
  }


  togglePassword(): void {
    this.showPassword = !this.showPassword;

    if(this.passwordToggleIcon = 'eye') {
      this.passwordToggleIcon = 'eye-off';
    }else {
      this.passwordToggleIcon = 'eye';
    }
  }

}
