import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SurveyHomeComponent } from './survey-home/survey-home.component';


const routes: Routes = [
  // { path: "authentication/plans", component: PlansComponent },
  // { path: "profile", component: ProfileComponent },
  // { path: "landing", component: LandingComponent },
  { path: "", component: SurveyHomeComponent },
  { path: "**", redirectTo: "", pathMatch: "full" },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})
export class SurveysRoutingModule { }
