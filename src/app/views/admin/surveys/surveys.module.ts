import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyHomeComponent } from './survey-home/survey-home.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { EditSurveyComponent } from './edit-survey/edit-survey.component';



@NgModule({
  declarations: [
    SurveyHomeComponent,
    CreateSurveyComponent,
    EditSurveyComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SurveysModule { }
